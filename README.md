# Zadatak 1

## Author:

Stefan Đorđević

## Run Commands:
```
run program:

npm start
```
```
run linter:

npm run lint
```
## File List:
```
.:

README.md

.eslintrc.json

package.json

./src
```
```
./src:

config.js

functions.js

index.js
```
