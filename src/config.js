'use strict';

// random:
const min = 4;
const max = 2;

// minIntegerFromArray:
const array = [12, 'asd', 23, 0, 'dwd', 213, 123, '12323', -1];

// minIntegerFromString:
const string = 'Danas, 17tog Septembra, je -12.52 i hladnije za 5.52 nego juce, 16og Septembra kada je bilo -7 0.';

// concatStringsByLength:
const arrayOfStrings = ['bb', 'a', 'ccc', 'dddd', 'eeeee', 'ffffff', 'ggggggg'];
const type = 1;

module.exports = {
	min,
	max,
	array,
	string,
	arrayOfStrings,
	type,
};
