'use strict';

const functions = require('./functions');
const data = require('./config');

console.log(functions.random(data.min, data.max));
console.log(functions.minIntegerFromArray(data.array));
console.log(functions.minIntegerFromString(data.string));
console.log(functions.concatStringsByLength(data.arrayOfStrings, data.type));
