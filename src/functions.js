'use strict';

const random = (min, max) => {
	if (typeof min !== 'number' || typeof max !== 'number') {
		return 'Entered values must be numbers.';
	}
	if (min > max) {
		return Math.floor(Math.random() * (min - max + 1)) + max;
	}
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

const minIntegerFromArray = (array) => {
	if (array.length <= 5 || !Array.isArray(array)) {
		return 'Please, enter an array with more than 5 members.';
	}
	const integers = array.filter(a => Number.isInteger(a));
	return integers.length ? Math.min(...integers) : false;
};

const minIntegerFromString = (string) => {
	if (string.length < 10 || typeof string !== 'string') {
		return 'Entered value must be a string at least 10 characters long.';
	}
	const numbers = string.match(/[+-]?\d+(?:\.\d+)?/g);
	const integers = numbers ? numbers.filter(num => Number.isInteger(Number(num))) : [];
	return integers.length ? Math.min(...integers) : false;
};

const concatStringsByLength = (arrayOfStrings, type) => {
	const compareNumbers = (type) ? (a, b) => b - a : (a, b) => a - b;
	return arrayOfStrings.sort((a, b) => compareNumbers(a.length, b.length)).join('');
};

module.exports = {
	random,
	minIntegerFromArray,
	minIntegerFromString,
	concatStringsByLength,
};
